<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaskBoard extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'user_id',
        'assigned_by',
        'assign_date',
        'due_date',
        'description'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function assignedBy()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function tasks()
    {
        return $this->hasMany('App\Models\Task', 'task_board_id');
    }
}
