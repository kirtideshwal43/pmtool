<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'task_board_id',
        'user_id',
        'assign_date',
        'completion_date',
        'due_date',
        'description'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function taskBoard()
    {
        return $this->belongsTo('App\Models\TaskBoard', 'task_board_id');
    }
}
