<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\User;
use Validator;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $rules = array(
            "email" => ['required','email'],
            "password" => ['required', 'min:8']
        );
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return $validator->errors();
        }

        $user = User::where('email', $request->email)->first();
        if(!$user || !Hash::check($request->password,$user->password))
        {
            return response([
                'message' => ['Invalid credentials !'],
            ], 203);
        }

        $token = $user->createToken('pmtool-token')->plainTextToken;

        $response = [
            'user' => $user,
            'token' => $token
        ];

        return response($response, 201);
    }

    public function register(Request $request)
    {
        $rules = array(
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        );
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return $validator->errors();
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        $token = $user->createToken('pmtool-token')->plainTextToken;

        $response = [
            'user' => $user,
            'token' => $token
        ];

        return response($response, 201);
    }

    public function logout(Request $request)
    {
        auth()->user()->tokens()->delete();
        $response = ["message"=>"You have been successfully logout."];
        return response($response, 200);
    }
}
