<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\TaskBoard;
use Validator;

class TaskBoardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }

    public function show(Request $request, $id)
    {
        $user = User::find($id);
        $taskboards = TaskBoard::where('user_id', $id)->get();
    
        return response(['user'=> $user,'taskboards' => $taskboards], 200);
    }

    public function store(Request $request)
    {
        $rules = array(
            "user_id" => ['required'],
            "title" => ['required','string'],
            "assign_date" => ['required'],
            "due_date" => ['required'],
        );
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return $validator->errors();
        }

        $taskboard = TaskBoard::create([
            'title' => $request->title,
            'user_id' => $request->user_id,
            'assigned_by' => auth()->user()->id,
            'assign_date' => $request->assign_date,
            'due_date' => $request->due_date,
            'description' => $request->description
        ]);
        
        return response(['taskboard'=>$taskboard],200);
    }

    public function delete(Request $request, $id)
    {
        TaskBoard::find($id)->delete();
        return response(["message"=>"taskboard deleted successfully"], 200);
    }

    public function getTaskBoardData(Request $request)
    {
        $rules = array(
            "id" => ['required'],
        );
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return $validator->errors();
        }

        $taskboard = TaskBoard::find($request->id);
        return response(['taskboard'=>$taskboard],200);
    }

    public function update(Request $request){
        $rules = array(
            "taskboard_id" => ["required"],
            "title" => ['required','string'],
            "assign_date" => ['required'],
            "due_date" => ['required'],
        );
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return $validator->errors();
        }

        $taskboard = TaskBoard::where('id', $request->taskboard_id)->update([
            'title' => $request->title,
            'assign_date' => $request->assign_date,
            'due_date' => $request->due_date,
            'description' => $request->description
        ]);

        return response(['message' => "taskboard updated successfully", 'taskboard'=> $taskboard], 200);
    }

}
