<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }

    public function show(Request $request, $id)
    {
        $taskboard = TaskBoard::find($id);
        $user = User::find($taskboard->user_id);
        $tasks = Task::where('task_board_id', $id)->get();
        return resonse(['user'=>$user, 'taskboard'=>$taskboard, 'tasks'=>$tasks],200);
    }

    public function getTaskData(Request $request)
    {
        $rules = array(
            "id" => ['required'],
        );
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return $validator->errors();
        }
        
        $task = Task::find($request->id);
        return response([
            'task' => $task
        ],200);
    }

    public function store(Request $request)
    {
        $rules = array(
            "user_id" => ['required'],
            "task_board_id" => ['required'],
            "title" => ['required','string'],
            "assign_date" => ['required'],
            "due_date" => ['required'],
        );
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return $validator->errors();
        }

        $task = TaskBoard::create([
            'title' => $request->title,
            'user_id' => $request->user_id,
            'task_board' => $request->user_id,
            'assign_date' => $request->assign_date,
            'due_date' => $request->due_date,
            'description' => $request->description
        ]);
        
        return response(['task'=>$task],200);
    }

    public function update()
    {
        $rules = array(
            "task_id" => ['required'],
            "title" => ['required','string'],
            "assign_date" => ['required'],
            "due_date" => ['required'],
        );
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return $validator->errors();
        }

        $task = Task::where('id', $request->task_id)->update([
            'title' => $request->title,
            'assign_date' => $request->assign_date,
            'due_date' => $request->due_date,
            'description' => $request->description
        ]);

        return response(['message' => "task updated successfully", 'task'=> $task], 200);
    }

    public function delete(Request $request, $id)
    {
        Task::find($id)->delete();
        return response(["message"=>"task deleted successfully"], 200);
    }
}
