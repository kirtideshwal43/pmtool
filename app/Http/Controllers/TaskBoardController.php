<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TaskBoard;
use DataTables;
use App\Models\User;

class TaskBoardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show(Request $request, $id)
    {
        $user = User::find($id);
        $boards = TaskBoard::where('user_id', $id)->get();
        if($request->ajax())
        {
            return Datatables::of($boards)
            ->addColumn('title', function($row){
                return ucwords($row->title);
            })
            ->addColumn('assign_date', function($row){
                return date('Y-m-d', strtotime($row->assign_date));
            })
            ->addColumn('due_date', function($row){
                return date('Y-m-d', strtotime($row->due_date));
            })
            ->addColumn('action', function($row){
                $actionbtn = '<div class="icon-group"><a class="btn btn-secondary" onclick="updateTaskBoardInfo('.$row->id.')" data-toggle="modal" data-target="#editTaskBoardModal"> Edit Task board </a> <a class="btn btn-primary" href="/tasks/'.$row->id.'"> View Tasks </a> <a class="btn btn-success" href="/create-task/'.$row->id.'"> Add Tasks </a> <a class="btn btn-danger" data-toggle="modal" onclick="'."$('#delete_taskboard_id').val(".$row->id.")".'" data-target="#deleteTaskBoardModal">Delete Task Board</a></div>';
                return $actionbtn;
            })
            ->rawColumns(['action'])->make(true);
        }
        return view('taskboard.show', compact('user'));
    }

    public function create(Request $request, $id)
    {
        $user = User::find($id);
        return view('taskboard.create', compact('user'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'bail|required|string',
            'assign_date' => 'bail|required',
            'due_date' => 'bail|required'
        ]);

        TaskBoard::create([
            'title' => $request->title,
            'user_id' => $request->user_id,
            'assigned_by' => auth()->user()->id,
            'assign_date' => $request->assign_date,
            'due_date' => $request->due_date,
            'description' => $request->description
        ]);

        return redirect(route('showTaskBoards', $request->user_id));
    }

    public function delete(Request $request)
    {
        TaskBoard::find($request->id)->delete();
        return redirect()->back();
    }

    public function getTaskBoardData(Request $request)
    {
        $taskboard = TaskBoard::find($request->id);
        return response()->json([
            'status' => 'success',
            'taskboard' => $taskboard
        ]);
    }

    public function update(Request $request)
    {
        $request->validate([
            'title' => 'bail|required|string',
            'assign_date' => 'bail|required',
            'due_date' => 'bail|required'
        ]);
        TaskBoard::where('id', $request->taskboard_id)->update([
            'title' => $request->title,
            'assign_date' => $request->assign_date,
            'due_date' => $request->due_date,
            'description' => $request->description
        ]);

        return redirect()->back();
    }

}
