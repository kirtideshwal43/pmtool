<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Models\User;    

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $users = User::all();
        if($request->ajax())
        {
            return Datatables::of($users)
            ->addColumn('name', function($row){
                return ucwords($row->name);
            })
            ->addColumn('email', function($row){
                return $row->email;
            })
            ->addColumn('action', function($row){
                $actionbtn = '<div class="icon-group"><a class="btn btn-primary" href="/task-boards/'.$row->id.'">View Task Board </a> <a href="/create-task-board/'.$row->id.'" class="btn btn-success">Create Task Board</a></div>';
                return $actionbtn;
            })
            ->rawColumns(['action'])->make(true);
        }
        return view('home', compact('users'));
    }
}
