<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TaskBoard;
use App\Models\Task;
use DataTables;
use App\Models\User;

class TaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show(Request $request, $id){
        $taskboard = TaskBoard::find($id);
        $user = User::find($taskboard->user_id);
        $tasks = Task::where('task_board_id', $id)->get();
        // dd($tasks);
        if($request->ajax())
        {
            return Datatables::of($tasks)
            ->addColumn('title', function($row){
                return ucwords($row->title);
            })
            ->addColumn('assign_date', function($row){
                return date('Y-m-d', strtotime($row->assign_date));
            })
            ->addColumn('due_date', function($row){
                return date('Y-m-d', strtotime($row->due_date));
            })
            ->addColumn('action', function($row){
                $actionbtn = '<div class="icon-group"><a class="btn btn-secondary" onclick="updateTaskInfo('.$row->id.')" data-toggle="modal" data-target="#editTaskModal"> Edit Task </a> <a class="btn btn-success" href="/task-boards/'.$row->id.'"> Add Tasks </a> <a class="btn btn-danger" data-toggle="modal" onclick="'."$('#delete_task_id').val(".$row->id.")".'" data-target="#deleteTaskModal">Delete Task</a></div>';
                return $actionbtn;
            })
            ->rawColumns(['action'])->make(true);
        }
        return view('task.show', compact('taskboard', 'user'));
    }

    public function create(Request $request, $id){
        $taskboard = TaskBoard::find($id);
        $user = User::find($taskboard->user_id);
        return view('task.create', compact('user', 'taskboard'));
    }

    public function store(Request $request){
        $request->validate([
            'title' => 'bail|required|string',
            'assign_date' => 'bail|required',
            'due_date' => 'bail|required'
        ]);
        Task::create([
            'title' => $request->title,
            'user_id' => $request->user_id,
            'task_board_id' => $request->task_board_id,
            'assign_date' => $request->assign_date,
            'due_date' => $request->due_date,
            'description' => $request->description
        ]);

        return redirect(route('showTasks', $request->task_board_id));
    }

    public function delete(Request $request)
    {
        Task::find($request->id)->delete();
        return redirect()->back();
    }

    public function getTaskData(Request $request)
    {
        $task = Task::find($request->id);
        return response()->json([
            'status' => 'success',
            'task' => $task
        ]);
    }

    public function update(Request $request)
    {
        $request->validate([
            'title' => 'bail|required|string',
            'assign_date' => 'bail|required',
            'due_date' => 'bail|required'
        ]);
        Task::where('id', $request->task_id)->update([
            'title' => $request->title,
            'assign_date' => $request->assign_date,
            'due_date' => $request->due_date,
            'description' => $request->description
        ]);

        return redirect()->back();
    }

}
