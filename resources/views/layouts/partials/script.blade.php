<script>
    $(document).ready(function(){
        $('#user_table').DataTable({
            bDestroy:true,
            processing: true,
            serverSide: true,
            ajax:{
                "url":"{{ url('/home') }}",
            },
            columns:[
                {data:'id', name:'id'},
                {data:'name', name:'name'},
                {data:'email', name:'email'},
                {data:'action', name:'action', orderable:false, searchable:false},

            ],
            fnRowCallback: function(nRow, aData, iDisplayIndex){
                $('td:first', nRow).html(iDisplayIndex +1);
                return nRow;
            }
        })

    })

    


</script>