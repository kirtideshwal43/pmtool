@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">{{ __('Task Boards') }} | {{ ucwords($user->name) }}</div>
        <div class="card-body">
            <table id="task_board_table">
                <thead>
                    <tr>
                        <th>Sr no.</th>
                        <th>Title</th>
                        <th>Assign Date</th>
                        <th>Due Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Modals -->
<!-- edit task board modal -->
<div class="modal fade" id="editTaskBoardModal" tabindex="-1" role="dialog" aria-labelledby="editTaskBoardModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editTaskBoardModalTitle">Edit Task Board</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('updateTaskBoard') }}" method="POST">
                @csrf
                    <input type="hidden" id="taskboard_id" name="taskboard_id">
                    <div class="form-group mb-3">
                        <label>Title</label>
                        <input class="form-control" type="text" name="title" id="taskboard_title" required>
                    </div>
                    <div class="form-group mb-3">
                        <label>Assign Data</label>
                        <input class="form-control" type="date" name="assign_date" id="taskboard_assign_date" required>
                    </div>
                    <div class="form-group mb-3">
                        <label>Due Date</label>
                        <input class="form-control" type="date" name="due_date" id="taskboard_due_date" required>
                    </div>
                    <div class="form-group mb-3">
                        <label>Description</label>
                        <textarea class="form-control" name="description" id="taskboard_description"></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-secondary form-control">Update</button>
                    </div>
                </form>
            </div>
            
        </div>
    </div>
</div>

<!-- delete task board modal -->
<div class="modal fade" id="deleteTaskBoardModal" tabindex="-1" role="dialog" aria-labelledby="deleteTaskBoardModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteTaskBoardModalTitle">Delete Task</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('deleteTaskBoard') }}" method="POST">
                @csrf
                    <div class="form-group">
                    <label>Are you sure you want to delete this task board ?</label>
                    <input type="hidden" name="id" id="delete_taskboard_id">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-danger form-control">Delete</button>
                    </div>
                </form>
            </div>
            
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    $(document).ready(function(){
        $('#task_board_table').DataTable({
            bDestroy:true,
            processing: true,
            serverSide: true,
            ajax:{
                "url":"{{ url('/task-boards', $user->id) }}",
            },
            columns:[
                {data:'id', name:'id'},
                {data:'title', name:'title'},
                {data:'assign_date', name:'assign_date'},
                {data:'due_date', name:'due_date'},
                {data:'action', name:'action', orderable:false, searchable:false},

            ],
            fnRowCallback: function(nRow, aData, iDisplayIndex){
                $('td:first', nRow).html(iDisplayIndex +1);
                return nRow;
            }
        })
    })

    function updateTaskBoardInfo(id)
    {
        $.ajax({
            url:"{{ route('getTaskBoardData') }}",
            data:{
                id:id,
                _token:'{{ csrf_token() }}'
            },
            success:function(response){
                console.log(response.taskboard)
                if(response.status == "success")
                {
                    $('#taskboard_id').val(response.taskboard.id)
                    $('#taskboard_title').val(response.taskboard.title)
                    $('#taskboard_assign_date').val(response.taskboard.assign_date.substring(0,10))
                    $('#taskboard_due_date').val(response.taskboard.due_date.substring(0,10))
                    $('#taskboard_description').html(response.taskboard.description)
                }
            },
            error:function(response){
                if(response.responseJSON.errors.hasOwnProperty("title"))
                {
                    $('#taskboard_title_error').text(response.responseJSON.errors.title[0])
                }
                if(response.responseJSON.errors.hasOwnProperty("assign_date"))
                {
                    $('#taskboard_assign_date_error').text(response.responseJSON.errors.assign_date[0])
                }
                if(response.responseJSON.errors.hasOwnProperty("due_date"))
                {
                    $('#taskboard_due_date_error').text(response.responseJSON.errors.due_date[0])
                }
            }
        })
    }
</script>
@endsection