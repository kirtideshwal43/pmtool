@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">{{ __('Tasks') }} | {{ucwords($taskboard->title)}} | {{ ucwords($user->name) }}</div>
        <div class="card-body">
            <table id="task_table">
                <thead>
                    <tr>
                        <th>Sr no.</th>
                        <th>Title</th>
                        <th>Assign Date</th>
                        <th>Due Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Modals -->
<!-- edit task modal -->
<div class="modal fade" id="editTaskModal" tabindex="-1" role="dialog" aria-labelledby="editTaskModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editTaskModalTitle">Edit Task</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('updateTask') }}" method="POST">
                @csrf
                    <input type="hidden" id="task_id" name="task_id">
                    <div class="form-group mb-3">
                        <label>Title</label>
                        <input class="form-control" type="text" name="title" id="task_title" required>
                    </div>
                    <div class="form-group mb-3">
                        <label>Assign Data</label>
                        <input class="form-control" type="date" name="assign_date" id="task_assign_date" required>
                    </div>
                    <div class="form-group mb-3">
                        <label>Due Date</label>
                        <input class="form-control" type="date" name="due_date" id="task_due_date" required>
                    </div>
                    <div class="form-group mb-3">
                        <label>Description</label>
                        <textarea class="form-control" name="description" id="task_description"></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-secondary form-control">Update</button>
                    </div>
                </form>
            </div>
            
        </div>
    </div>
</div>

<!-- delete task modal -->
<div class="modal fade" id="deleteTaskModal" tabindex="-1" role="dialog" aria-labelledby="deleteTaskModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteTaskModalTitle">Delete Task</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('deleteTask') }}" method="POST">
                @csrf
                    <div class="form-group">
                    <label>Are you sure you want to delete this task ?</label>
                    <input type="hidden" name="id" id="delete_task_id">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-danger form-control">Delete</button>
                    </div>
                </form>
            </div>
            
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    $(document).ready(function(){
        $('#task_table').DataTable({
            bDestroy:true,
            processing: true,
            serverSide: true,
            ajax:{
                "url":"{{ url('/tasks', $taskboard->id) }}",
            },
            columns:[
                {data:'id', name:'id'},
                {data:'title', name:'title'},
                {data:'assign_date', name:'assign_date'},
                {data:'due_date', name:'due_date'},
                {data:'action', name:'action', orderable:false, searchable:false},

            ],
            fnRowCallback: function(nRow, aData, iDisplayIndex){
                $('td:first', nRow).html(iDisplayIndex +1);
                return nRow;
            }
        })
    })

    function updateTaskInfo(id)
    {
        $.ajax({
            url:"{{ route('getTaskData') }}",
            data:{
                id:id,
                _token:'{{ csrf_token() }}'
            },
            success:function(response){
                console.log(response.taskboard)
                if(response.status == "success")
                {
                    $('#task_id').val(response.task.id)
                    $('#task_title').val(response.task.title)
                    $('#task_assign_date').val(response.task.assign_date.substring(0,10))
                    $('#task_due_date').val(response.task.due_date.substring(0,10))
                    $('#task_description').html(response.task.description)
                }
            },
            error:function(response){
                if(response.responseJSON.errors.hasOwnProperty("title"))
                {
                    $('#taskboard_title_error').text(response.responseJSON.errors.title[0])
                }
                if(response.responseJSON.errors.hasOwnProperty("assign_date"))
                {
                    $('#taskboard_assign_date_error').text(response.responseJSON.errors.assign_date[0])
                }
                if(response.responseJSON.errors.hasOwnProperty("due_date"))
                {
                    $('#taskboard_due_date_error').text(response.responseJSON.errors.due_date[0])
                }
            }
        })
    }

</script>
@endsection