@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">{{ ucwords($taskboard->title) }} | {{ ucwords($user->name) }}</div>
        <div class="card-body">
            <form action="{{ route('saveTask') }}" method="POST">
            @csrf
                <input type='hidden' name="user_id" value="{{$user->id}}">
                <input type='hidden' name="task_board_id" value="{{$taskboard->id}}">
                <div class="form-group mb-3">
                    <label>Title</label>
                    <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" required>
                    @error('title')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group mb-3">
                    <label>Assign Date</label>
                    <input type="date" class="form-control @error('assign_date') is-invalid @enderror" name="assign_date" required>
                    @error('assign_date')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group mb-3">
                    <label>Due Date</label>
                    <input type="date" class="form-control @error('due_date') is-invalid @enderror" name="due_date" required>
                    @error('due_date')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group mb-3">
                    <label>Description</label>
                    <textarea class="form-control" name="description"></textarea>
                </div>
                <div class="form-group mt-4">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection