<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post("login", [App\Http\Controllers\api\UserController::class, 'index']);
Route::post("register", [App\Http\Controllers\api\UserController::class, 'register']);
Route::post("logout", [App\Http\Controllers\api\UserController::class, 'logout']);


Route::get("taskboards/{id}", [App\Http\Controllers\api\TaskBoardController::class, 'show']);
Route::get("getTaskBoardData", [App\Http\Controllers\api\TaskBoardController::class, 'getTaskBoardData']);
Route::post("create-taskboard", [App\Http\Controllers\api\TaskBoardController::class, 'store']);
Route::update("update-taskboard", [App\Http\Controllers\api\TaskBoardController::class, 'update']);
Route::delete("delete-taskboard/{id}", [App\Http\Controllers\api\TaskBoardController::class, 'delete']);


Route::get("tasks/{id}", [App\Http\Controllers\api\TaskBoardController::class, 'show']);
Route::get("getTaskData", [App\Http\Controllers\api\TaskBoardController::class, 'getTaskData']);
Route::post("create-task", [App\Http\Controllers\api\TaskBoardController::class, 'store']);
Route::update("update-task", [App\Http\Controllers\api\TaskBoardController::class, 'update']);
Route::delete("delete-task/{id}", [App\Http\Controllers\api\TaskBoardController::class, 'delete']);


