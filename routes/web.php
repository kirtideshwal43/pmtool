<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/task-boards/{id}', [App\Http\Controllers\TaskBoardController::class, 'show'])->name('showTaskBoards');
Route::get('/create-task-board/{id}', [App\Http\Controllers\TaskBoardController::class, 'create'])->name('createTaskBoard');
Route::post('/save-task-board', [App\Http\Controllers\TaskBoardController::class, 'store'])->name('saveTaskBoard');
Route::post('/delete-task-board', [App\Http\Controllers\TaskBoardController::class, 'delete'])->name('deleteTaskBoard');
Route::get('/get-task-board-data', [App\Http\Controllers\TaskBoardController::class, 'getTaskBoardData'])->name('getTaskBoardData');
Route::post('/update-task-board', [App\Http\Controllers\TaskBoardController::class, 'update'])->name('updateTaskBoard');

Route::get('/tasks/{id}', [App\Http\Controllers\TaskController::class, 'show'])->name('showTasks');
Route::get('/create-task/{id}', [App\Http\Controllers\TaskController::class, 'create'])->name('createTask');
Route::post('/save-task', [App\Http\Controllers\TaskController::class, 'store'])->name('saveTask');
Route::post('/delete-task', [App\Http\Controllers\TaskController::class, 'delete'])->name('deleteTask');
Route::get('/get-task-data', [App\Http\Controllers\TaskController::class, 'getTaskData'])->name('getTaskData');
Route::post('/update-task', [App\Http\Controllers\TaskController::class, 'update'])->name('updateTask');
